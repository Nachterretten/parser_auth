package docs

import (
	ucontroller "gitlab.com/Nachterretten/goboilerplate/internal/modules/user/controller"
	"gitlab.com/Nachterretten/goboilerplate/internal/modules/user/service"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
// - Bearer: []
// responses:
// 200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}

// swagger:route POST /api/1/user/profile/change_password user changePasswordRequest
// Смена пароля пользователя.
// security:
// - Bearer: []
// responses:
// 200: changePasswordResponse

// swagger: parameters changePasswordRequest
type changePasswordRequest struct {
	// in:body
	Body service.ChangePasswordIn
}

// swagger:response changePasswordResponse
type changePasswordResponse struct {
	// in:body
	Body struct {
		Success   bool `json:"success"`
		ErrorCode int  `json:"error_code"`
	}
}

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

package controller

import (
	"encoding/json"
	"github.com/ptflp/godecoder"
	"gitlab.com/Nachterretten/goboilerplate/internal/infrastructure/component"
	"gitlab.com/Nachterretten/goboilerplate/internal/infrastructure/errors"
	"gitlab.com/Nachterretten/goboilerplate/internal/infrastructure/handlers"
	"gitlab.com/Nachterretten/goboilerplate/internal/infrastructure/responder"
	"gitlab.com/Nachterretten/goboilerplate/internal/modules/user/service"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	// Разбираем входные данные из запроса, используя декодер json.NewDecoder
	var changePasswordIn service.ChangePasswordIn
	err := json.NewDecoder(r.Body).Decode(&changePasswordIn)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Вызываем метод ChangePassword сервиса, передавая входные данные
	out := u.service.ChangePassword(r.Context(), changePasswordIn)

	// Обрабатываем результат и возвращаем ответ в JSON формате
	response := struct {
		Success   bool `json:"success"`
		ErrorCode int  `json:"error_code"`
	}{
		Success:   out.Success,
		ErrorCode: out.ErrorCode,
	}

	responseJSON, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(responseJSON)
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

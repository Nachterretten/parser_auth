package main

// КЭШ РЕАЛИЗОВАЛ ДЛЯ БД ПОСТГРЕС В СЕРВИСЕ. Глянь пожалуйста и скажи, что можно исправить
import (
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"parser/internal/config"

	db2 "parser/internal/db"

	"parser/internal/handlers"

	"parser/internal/repository"

	"parser/internal/service"

	"log"
	"time"

	_ "github.com/lib/pq"
	"github.com/tebeka/selenium"

	"parser/internal/driver"
)

func main() {
	cfg := config.NewConfig()
	dbConf := config.NewDB()
	mongoConfig := config.NewMongoDB()

	wd, err := driver.CreateWebDriver()
	if err != nil {
		log.Fatalf("Failed to create WebDriver: %v", err)
	}
	defer func(wd selenium.WebDriver) {
		err := wd.Quit()
		if err != nil {
			log.Fatalf("Невозжно подключиться к селениуму")
		}
	}(wd)

	// Создаем экземпляры репозитория, сервиса
	vacancyRepo := repository.NewVacancyRepository(wd)
	vacancyService := service.NewVacancyService(vacancyRepo)

	mongoRepo := repository.NewMongoDB(wd, *mongoConfig)
	mongoService := service.NewMongoVacancyService(mongoRepo)

	db, err := db2.NewSqlDB(*dbConf)
	if err != nil {
		log.Fatalf("Failed to connect database: %v", err)
	}
	defer func(db *sqlx.DB) {
		err := db.Close()
		if err != nil {

		}
	}(db)

	// создаем нвоый клиент редис
	client := redis.NewClient(&redis.Options{Addr: "localhost:6379"})

	pgsqlRepo := repository.NewPostgresRepository(wd, db)
	pgsqlVacancyService := service.NewPostgresService(pgsqlRepo, wd, client)

	// Создаем экземпляр сервера API
	apiServer := handlers.New(cfg, vacancyService, dbConf, pgsqlVacancyService, mongoConfig, mongoService)

	// Запускаем сервер API
	go func() {
		router := handlers.SetupRouter(apiServer)
		err := router.Run(cfg.HTTPAddr)
		if err != nil {
			log.Fatalf("failed to start API server: %v", err)
		}
	}()
	// Ждем 150 секунд, чтобы успеть посмотреть результат
	time.Sleep(10 * time.Minute)
}

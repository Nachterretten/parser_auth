package config

type Config struct {
	HTTPAddr string `toml:"bind_addr"`
}

func NewConfig() *Config {
	return &Config{
		HTTPAddr: ":8080",
	}
}

type DB struct {
	Driver   string
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
}

func NewDB() *DB {
	return &DB{
		Driver:   "postgres",
		Host:     "pgdb",
		Port:     "5432",
		User:     "root",
		Password: "qwerty",
		DBName:   "root",
	}
}

type MongoDB struct {
	Host     string
	Port     string
	DBName   string
	User     string
	Password string
}

func NewMongoDB() *MongoDB {
	return &MongoDB{
		Host:     "mongodb",
		Port:     "27017",
		User:     "root",
		Password: "qwerty",
		DBName:   "mongodb",
	}
}

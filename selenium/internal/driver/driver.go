package driver

import (
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/firefox"
	"log"
	"time"
)

const maxTries = 15

func CreateWebDriver() (selenium.WebDriver, error) {
	// Configure the driver
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}
	// Add Firefox settings to the driver configuration
	fxOpts := firefox.Capabilities{}
	caps.AddFirefox(fxOpts)

	// Create the WebDriver instance
	var wd selenium.WebDriver
	var err error

	urlPrefix := "http://selenium1:4444"

	// Retry a few times to create the WebDriver
	for i := 0; i < maxTries; i++ {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err == nil {
			break
		}
		log.Println(err)
		time.Sleep(1 * time.Second)
	}

	return wd, err
}

package handlers

import (
	"github.com/gin-gonic/gin"

	"parser/internal/domain"

	"html/template"
	"net/http"
	"strconv"
	"time"
)

const swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
          url: "public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`

func (s *APIServer) CreateHandler(c *gin.Context) {
	// Получите токен доступа из контекста Gin
	sessionToken, _ := c.Get("session_token")

	// Проверьте авторизацию с помощью isAuthorized
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	var vacancy domain.Vacancy
	if err := c.ShouldBindJSON(&vacancy); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := s.service.Create(&vacancy); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.Status(http.StatusCreated)
}

func (s *APIServer) DeleteHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	if err := s.service.Delete(id); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

func (s *APIServer) GetListHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	// Проверьте авторизацию с помощью isAuthorized
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	vacancies, err := s.service.GetList()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, vacancies)
}

func (s *APIServer) GetByIDHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	vacancy, err := s.service.GetByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, vacancy)
}

func (s *APIServer) SwaggerUI(c *gin.Context) {
	c.Header("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		c.String(http.StatusInternalServerError, "Failed to load Swagger UI")
		return
	}
	err = tmpl.Execute(c.Writer, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		c.String(http.StatusInternalServerError, "Failed to render Swagger UI")
		return
	}
}

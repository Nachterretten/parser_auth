package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"parser/internal/domain"
	"time"

	"net/http"
	"strconv"
)

type AuthResponse struct {
	Success bool `json:"success"`
	Data    struct {
		AccessToken  string `json:"access_token"`
		RefreshToken string `json:"refresh_token"`
		Message      string `json:"message"`
	} `json:"data"`
}

const (
	authURL = "http://sampleapi:8085/api/1/auth/login"
)

func authenticateUser(email, password string) (string, error) {
	// Создаем структуру для передачи данных авторизации в микросервис
	authData := map[string]string{
		"email":    email,
		"password": password,
	}

	// Кодируем данные в JSON
	jsonData, err := json.Marshal(authData)
	if err != nil {
		return "", err
	}

	// Создаем запрос POST для авторизации с email и password
	resp, err := http.Post(authURL, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Читаем тело ответа
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	// Парсим ответ
	var authResp AuthResponse
	if err := json.Unmarshal(body, &authResp); err != nil {
		return "", err
	}

	// Проверяем, успешна ли авторизация
	if !authResp.Success {
		return "", fmt.Errorf("авторизация неуспешна: %s", authResp.Data.Message)
	}

	// Возвращаем токен доступа, если авторизация успешна
	return authResp.Data.AccessToken, nil
}

func isAuthorized(accessToken string) bool {
	// Выполняем проверку наличия токена доступа или его валидности.
	// В этом примере просто проверяем, что токен доступа не пустой.
	if accessToken == "" {
		return false
	}
	return true
}

func (s *APIServer) LoginHandler(c *gin.Context) {
	// Создаем структуру для хранения данных из запроса
	var loginRequest struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	// Получаем данные из JSON запроса и связываем их с структурой loginRequest
	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	accessToken, err := authenticateUser(loginRequest.Email, loginRequest.Password)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	// Установите cookie с токеном доступа
	cookie := &http.Cookie{
		Name:     "session_token",
		Value:    accessToken,
		Path:     "/",
		Domain:   "localhost",
		Expires:  time.Now().Add(1 * time.Hour),
		HttpOnly: true,
		Secure:   false,
		SameSite: http.SameSiteStrictMode,
	}

	http.SetCookie(c.Writer, cookie)

	// Отправляем успешный ответ, если авторизация прошла успешно
	c.JSON(http.StatusOK, gin.H{"success": true, "message": "Успешная авторизация"})
}

func (s *APIServer) PGSQLCreateHandler(c *gin.Context) {
	// Получите токен доступа из контекста Gin
	sessionToken, _ := c.Get("session_token")

	// Проверьте авторизацию с помощью isAuthorized
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	var vacancy domain.Vacancy
	if err := c.ShouldBindJSON(&vacancy); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := s.servicePgsql.CreateVacancy(vacancy); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.Status(http.StatusCreated)
}

func (s *APIServer) PGSQLDeleteHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	if err := s.servicePgsql.DeleteVacancy(id); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

func (s *APIServer) PGSQLGetListHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	vacancies, err := s.servicePgsql.GetVacancyList()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, vacancies)
}

func (s *APIServer) PGSQLGetByIDHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	idStr := c.Param("id")

	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	vacancy, err := s.servicePgsql.GetVacancyByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, vacancy)
}

func (s *APIServer) SearchHandler(c *gin.Context) {
	// Получаем токен доступа
	sessionToken, _ := c.Get("session_token")

	// Проверяем авторизацию
	if !isAuthorized(sessionToken.(string)) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}

	var request struct {
		Query string `json:"query" binding:"required"`
	}
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	query := request.Query

	err := s.servicePgsql.Search(query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

package handlers

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
	"parser/internal/config"
	"parser/internal/service"
	"syscall"
	"time"
)

type APIServer struct {
	config       *config.Config
	service      *service.VacancyService
	servicePgsql *service.PostgresService
	serviceMongo *service.MongoVacancyService
	db           *config.DB
	mongoCfg     *config.MongoDB
}

func New(config *config.Config, vacancyService *service.VacancyService, db *config.DB, psgql *service.PostgresService, mongoCfg *config.MongoDB, mongoVacancyService *service.MongoVacancyService) *APIServer {
	return &APIServer{
		config:       config,
		service:      vacancyService,
		servicePgsql: psgql,
		db:           db,
		mongoCfg:     mongoCfg,
		serviceMongo: mongoVacancyService,
	}
}

func CheckSession() gin.HandlerFunc {
	return func(c *gin.Context) {
		sessionID, err := c.Cookie("session_token")
		if err != nil {
			// Если cookie не найден, пользователь не авторизован
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// Проверьте валидность и актуальность sessionID
		// Возможно, вам понадобится дополнительная логика для проверки сессии

		// Установите sessionID в контекст для дальнейшего использования
		c.Set("session_token", sessionID)

		// Продолжите выполнение запроса
		c.Next()
	}
}

func (s *APIServer) Run() {
	server := &http.Server{
		Addr: s.config.HTTPAddr,
	}

	log.Printf("Listening on port %s", s.config.HTTPAddr)

	idConnClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(ctx); err != nil {
			log.Fatalln(err)
		}
		close(idConnClosed)
	}()

	if err := server.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			log.Fatalln(err)
		}
	}
	<-idConnClosed
	log.Println("Server stopped")
}

func SetupRouter(s *APIServer) *gin.Engine {
	router := gin.Default()

	// Обработчик для отображения Swagger UI
	router.GET("/swagger", s.SwaggerUI)

	// Обработчик авторизации
	router.POST("/login", s.LoginHandler)

	// Обработчик для статических файлов в папке "/public/"
	router.GET("/public/*filepath", func(c *gin.Context) {
		c.File("./public/" + c.Param("filepath"))
	})

	// Все остальные эндпоинты будут проходить проверку на наличие сессии
	authGroup := router.Group("/")
	authGroup.Use(CheckSession())
	{
		authGroup.POST("/vacancies", s.CreateHandler)
		authGroup.DELETE("/vacancies/:id", s.DeleteHandler)
		authGroup.GET("/vacancies", s.GetListHandler)
		authGroup.GET("/vacancies/:id", s.GetByIDHandler)

		// Обработчик для запросов к базе данных
		authGroup.GET("/db/vacancy/:id", s.PGSQLGetByIDHandler)
		authGroup.GET("/db/vacancies", s.PGSQLGetListHandler)
		authGroup.POST("/db/vacancy", s.PGSQLCreateHandler)
		authGroup.DELETE("/db/vacancy/:id", s.PGSQLDeleteHandler)
		authGroup.POST("/db/vacanciessave", s.SearchHandler)

		// Обработчик для запросов к базе данных Mongo
		authGroup.GET("/mongo/vacancy/:id", s.MongoGetByIDHandler)
		authGroup.GET("/mongo/vacancies", s.MongoGetListHandler)
		authGroup.POST("/mongo/vacancy", s.MongoCreateHandler)
		authGroup.DELETE("/mongo/vacancy/:id", s.MongoDeleteHandler)
		authGroup.POST("/mongo/vacanciessave", s.MongoSearchHandler)
	}

	return router
}

package repository

import (
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"parser/internal/config"

	"parser/internal/domain"

	"log"
	"net/http"
	"time"
)

// MongoRepositoryStorager интерфейс для работы с вакансиями
type MongoRepositoryStorager interface {
	Create(vacancy domain.Vacancy) error
	GetByID(id int) (*domain.Vacancy, error)
	GetList() ([]domain.Vacancy, error)
	Delete(id int) error
	Search(query string) error
}

///

// MongoRepository представляет репозиторий для работы с вакансиями
type MongoRepository struct {
	wd         selenium.WebDriver
	client     *mongo.Client
	collection *mongo.Collection
	config     *config.MongoDB
}

// NewMongoDB создает новый экземпляр MongoRepository
func NewMongoDB(wd selenium.WebDriver, db config.MongoDB) *MongoRepository {
	// Создаем подключение к MongoDB
	connectionString := fmt.Sprintf("mongodb://%s:%s@%s:%s", db.User, db.Password, db.Host, db.Port)
	clientOptions := options.Client().ApplyURI(connectionString)
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatalf("Failed to connect to MongoDB: %v", err)
	}

	// Проверяем подключение к базе данных MongoDB
	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatalf("Failed to ping MongoDB: %v", err)
	}

	// Получаем ссылку на коллекцию вакансий
	collection := client.Database(db.DBName).Collection("vacancies")

	return &MongoRepository{
		wd:         wd,
		client:     client,
		collection: collection,
		config:     &db,
	}
}

func (m *MongoRepository) Create(vacancy domain.Vacancy) error {
	// выполняем вставку документа в коллекцию
	_, err := m.collection.InsertOne(context.Background(), vacancy)
	if err != nil {
		return fmt.Errorf("failed to create vacancy: %v", err)
	}
	return nil
}

func (m *MongoRepository) GetByID(id int) (*domain.Vacancy, error) {
	// создаем фильтр для поиска по полю id
	filter := bson.M{"vacancy_id": id}
	// выполянем поиск по фильтру
	var vacancy domain.Vacancy
	err := m.collection.FindOne(context.Background(), filter).Decode(&vacancy)
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancy by ID: %v", err)
	}
	return &vacancy, nil
}

func (m *MongoRepository) GetList() ([]domain.Vacancy, error) {
	// выполняем поиск всех документов в коллекции
	cursor, err := m.collection.Find(context.Background(), bson.M{})
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancies list: %v", err)
	}
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		err := cursor.Close(ctx)
		if err != nil {

		}
	}(cursor, context.Background())
	// инициализируем слайс для хранения результатов
	var vacancies []domain.Vacancy
	// перебираем результаты и декодируем каждый документ в структуру Vacancy
	for cursor.Next(context.Background()) {
		var vacancy domain.Vacancy
		if err != nil {
			return nil, fmt.Errorf("failed to decode vacancy: %v", err)
		}
		vacancies = append(vacancies, vacancy)
	}
	return vacancies, err
}

func (m *MongoRepository) Delete(id int) error {
	// создаем фильтр для удаления по полю id
	filter := bson.M{"vacancy_id": id}
	// выполняем удаление по фильтру
	_, err := m.collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return fmt.Errorf("failed to delete vacancy: %v", err)
	}
	return nil
}

func (m *MongoRepository) Search(query string) error {
	rootURL := "https://career.habr.com"
	var links []string
	var page int

	for {
		page++
		// выполняем запрос на страницу с результатами поиска ваканссии
		err := m.wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
		if err != nil {
			return fmt.Errorf("failed to load search results page: %v", err)
		}
		// проверяем наличине элемента search-total, чтобы убедиться что мы на странице с результатом поиска
		// если элемент не найден - выходим их цикла
		_, err = m.wd.FindElements(selenium.ByCSSSelector, ".search-total")
		if err != nil {
			break
		}
		// находим все элементы на странице соответсвующие ".vacancy-vard__title-link"
		// для каждого элемента получаем href, который яв-ся ссылкой
		elems, err := m.wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return fmt.Errorf("не удалось найти ссылки на вакансии: %v", err)
		}
		for _, elem := range elems {
			link, err := elem.GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, rootURL+link)
		}
	}
	log.Printf("total vacancies - %v", len(links))

	var vacancy domain.Vacancy

	// загружаем страницу для каждой ссылки
	for _, link := range links {
		resp, err := http.Get(link)
		if err != nil {
			log.Printf("%v", err)
			continue
		}
		// разбираем содержимое страницы
		var doc *goquery.Document
		doc, err = goquery.NewDocumentFromReader(resp.Body)
		if err != nil || doc == nil {
			log.Printf("%v", err)
			continue
		}

		// находим элементы с атрибутом type="application/ld+json"
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			log.Printf("%v", err)
			continue
		}
		// декодируем первый элемент из найденных элементов
		vacancy, err = domain.UnmarshalVacancy([]byte(dd.First().Text()))
		if err != nil {
			return fmt.Errorf("couldn't make out the details of the vacancy: %v", err)
		}
		// передаем значение в метод Create для сохранения вакансии
		err = m.Create(vacancy)
		if err != nil {
			return fmt.Errorf("failed to save vacancy: %v", err)
		}
		time.Sleep(1 * time.Second)
	}
	return nil
}

package repository

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/jmoiron/sqlx"
	"github.com/tebeka/selenium"

	"parser/internal/domain"

	"log"
	"net/http"
	"sync"
	"time"
)

type RepositoryStorager interface {
	Create(vacancy domain.Vacancy) error
	GetByID(id int) (*domain.Vacancy, error)
	GetList() ([]domain.Vacancy, error)
	Delete(id int) error
	Search(query string) error
}

// PostgresRepository представляет репозиторий для работы с вакансиями
type PostgresRepository struct {
	wd selenium.WebDriver
	db *sqlx.DB
	sync.Mutex
}

// NewPostgresRepository создает новый экземпляр VacancyRepository
func NewPostgresRepository(wd selenium.WebDriver, db *sqlx.DB) *PostgresRepository {
	return &PostgresRepository{
		wd: wd,
		db: db,
	}
}

func (p *PostgresRepository) Create(vacancy domain.Vacancy) error {
	// SQL-запрос
	query := `
		INSERT INTO vacancies (vacancy_id, organization, title, description, date_posted, valid_through, employment_type)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	// Выполняем запрос
	_, err := p.db.Exec(query, vacancy.Value, vacancy.Name, vacancy.Title, vacancy.Description, vacancy.DatePosted, vacancy.ValidThrough, vacancy.EmploymentType)
	if err != nil {
		return fmt.Errorf("failed to create vacancy: %v", err)
	}

	return nil
}

func (p *PostgresRepository) GetByID(id int) (*domain.Vacancy, error) {
	// SQL-запрос
	query := "SELECT * FROM vacancies WHERE id = $1"

	// Выполняем запрос
	var vacancy domain.Vacancy
	err := p.db.Get(&vacancy, query, id)
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancy by ID: %v", err)
	}

	return &vacancy, nil
}

func (p *PostgresRepository) GetList() ([]domain.Vacancy, error) {
	// sql запрос
	query := "SELECT * FROM vacancies"
	// выполняем запрос
	var vacancies []domain.Vacancy
	err := p.db.Select(&vacancies, query)
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancies list: %v", err)
	}
	return vacancies, nil
}

func (p *PostgresRepository) Delete(id int) error {
	// sql запрос
	query := "DELETE FROM vacancies WHERE id = $1"

	_, err := p.db.Exec(query, id)
	if err != nil {
		return fmt.Errorf("failed to delete vacancy: %v", err)
	}
	return nil
}

func (p *PostgresRepository) Search(query string) error {
	rootURL := "https://career.habr.com"
	var links []string
	var page int

	for {
		page++
		// Execute the request to the page with job search results
		err := p.wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
		if err != nil {
			return fmt.Errorf("failed to load search results page: %v", err)
		}

		// Check for the presence of an element with the class "search-total" to ensure that we are on the search results page.
		// If the element is not found, exit the loop.
		_, err = p.wd.FindElement(selenium.ByCSSSelector, ".search-total")
		if err != nil {
			break
		}

		// Find all elements on the page that match the CSS selector ".vacancy-card__title-link".
		// Then, for each element, get the value of the "href" attribute, which contains the link to the job vacancy.
		elems, err := p.wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return fmt.Errorf("failed to find vacancy links: %v", err)
		}
		for _, elem := range elems {
			link, err := elem.GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, rootURL+link)
		}
	}

	log.Printf("Total vacancies - %v", len(links))

	var vacancy domain.Vacancy

	// Load the page for each link
	for _, link := range links {
		resp, err := http.Get(link)
		if err != nil {
			log.Printf("%v", err)
			continue
		}
		// Parse the body of the page
		var doc *goquery.Document
		doc, err = goquery.NewDocumentFromReader(resp.Body)
		if err != nil || doc == nil {
			log.Printf("%v", err)
			continue
		}

		// Find elements with the attribute type="application/ld+json".
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			log.Printf("%v", err)
			continue
		}

		// Decode the first element from the found elements, converting the value to a byte slice
		vacancy, err = domain.UnmarshalVacancy([]byte(dd.First().Text()))
		if err != nil {
			return fmt.Errorf("failed to parse vacancy details: %v", err)
		}

		// Pass the value to the Create method to save the vacancy
		err = p.Create(vacancy)
		if err != nil {
			return fmt.Errorf("failed to save vacancy: %v", err)
		}

		time.Sleep(1 * time.Second)
	}

	return nil
}

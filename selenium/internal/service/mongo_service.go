package service

import (
	"parser/internal/domain"
	"parser/internal/repository"
)

// MongoVacancyService представляет сервис для работы с вакансиями.
type MongoVacancyService struct {
	repo repository.MongoRepositoryStorager
}

// NewMongoVacancyService создает новый экземпляр VacancyService.
func NewMongoVacancyService(repo repository.MongoRepositoryStorager) *MongoVacancyService {
	return &MongoVacancyService{
		repo: repo,
	}
}

// CreateVacancy создает новую вакансию.
func (s *MongoVacancyService) CreateVacancy(vacancy domain.Vacancy) error {
	return s.repo.Create(vacancy)
}

// GetVacancyByID возвращает вакансию по указанному идентификатору.
func (s *MongoVacancyService) GetVacancyByID(id int) (*domain.Vacancy, error) {
	return s.repo.GetByID(id)
}

// GetVacancyList возвращает список всех вакансий.
func (s *MongoVacancyService) GetVacancyList() ([]domain.Vacancy, error) {
	return s.repo.GetList()
}

// DeleteVacancyByID удаляет вакансию по указанному идентификатору.
func (s *MongoVacancyService) DeleteVacancyByID(id int) error {
	return s.repo.Delete(id)
}

// SearchVacancies выполняет поиск вакансий по указанному запросу.
func (s *MongoVacancyService) SearchVacancies(query string) error {
	return s.repo.Search(query)
}

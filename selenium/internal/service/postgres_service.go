package service

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/tebeka/selenium"
	"parser/internal/domain"
	"parser/internal/repository"
)

// PostgresService представляет сервис для работы с вакансиями
type PostgresService struct {
	redis       *redis.Client
	vacancyRepo repository.RepositoryStorager
	wd          selenium.WebDriver
}

// NewPostgresService создает новый экземпляр PostgresService
func NewPostgresService(vacancyRepo repository.RepositoryStorager, wd selenium.WebDriver, redis *redis.Client) *PostgresService {
	return &PostgresService{
		vacancyRepo: vacancyRepo,
		wd:          wd,
		redis:       redis,
	}
}

// CreateVacancy создает новую вакансию
func (s *PostgresService) CreateVacancy(vacancy domain.Vacancy) error {
	err := s.vacancyRepo.Create(vacancy)
	if err == nil {
		// удаляем данные из кеша после создания вакансии для обновления
		s.clearCache()
	}
	return nil
}

// GetVacancyByID возвращает вакансию по заданному идентификатору
func (s *PostgresService) GetVacancyByID(id int) (*domain.Vacancy, error) {
	// сначала пробуем получить данные из кеша
	vacancy, err := s.getVacancyFromCache(id)
	if err != nil {
		// если данных нет в кэше получаем из репозитория
		vacancy, err = s.vacancyRepo.GetByID(id)
	}
	if err == nil {
		// сохраняем данные в кэш для последующих запросов
		err = s.setVacancyToCache(id, vacancy)
	}

	return vacancy, nil
}

// GetVacancyList возвращает список всех вакансий
func (s *PostgresService) GetVacancyList() ([]domain.Vacancy, error) {
	// пробуем получить список всех вакансии
	vacancies, err := s.getVacancyListFromCache()
	if err != nil {
		// если там нет данных, то получаем из репозитория
		vacancies, err = s.vacancyRepo.GetList()
		if err != nil {
			// сохраняем данные для следующих запросов
			err = s.setVacancyListToCache(vacancies)
		}
	}
	return vacancies, nil
}

// DeleteVacancy удаляет вакансию по заданному идентификатору
func (s *PostgresService) DeleteVacancy(id int) error {
	err := s.vacancyRepo.Delete(id)
	if err == nil {
		// удаляем данные из кэшеа
		s.clearCache()
	}
	return nil
}

func (s *PostgresService) Search(query string) error {
	// т.к поиск может изменить данные в репозитории очищаем кэш
	s.clearCache()
	return s.vacancyRepo.Search(query)
}

// getVacancyFromCache получает вакансию из кеша
func (s *PostgresService) getVacancyFromCache(id int) (*domain.Vacancy, error) {
	key := fmt.Sprintf("vacancy:%d", id)
	cmd := s.redis.Get(context.TODO(), key)
	if cmd.Err() == redis.Nil {
		return nil, redis.Nil
	} else if cmd.Err() != nil {
		return nil, cmd.Err()
	}

	var vacancy domain.Vacancy
	err := json.Unmarshal([]byte(cmd.Val()), &vacancy)
	if err != nil {
		return nil, err
	}
	return &vacancy, nil
}

// setVacancyToCache сохраняет вакансии в кэш по ID
func (s *PostgresService) setVacancyToCache(id int, vacancy *domain.Vacancy) error {
	key := fmt.Sprintf("vacancy:%d", id)
	data, err := json.Marshal(vacancy)
	if err != nil {
		return err
	}

	err = s.redis.Set(context.TODO(), key, data, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

// getVacancyListFromCache получаем список вакансии из кэша
func (s *PostgresService) getVacancyListFromCache() ([]domain.Vacancy, error) {
	cmd := s.redis.Get(context.TODO(), "vacancy_list")
	if cmd.Err() == redis.Nil {
		return nil, redis.Nil
	} else if cmd.Err() != nil {
		return nil, cmd.Err()
	}

	var vacancies []domain.Vacancy
	err := json.Unmarshal([]byte(cmd.Val()), &vacancies)
	if err != nil {
		return nil, err
	}

	return vacancies, nil
}

// setVacancyListToCache сохраняет список вакансии в кэше
func (s *PostgresService) setVacancyListToCache(vacancies []domain.Vacancy) error {
	data, err := json.Marshal(vacancies)
	if err != nil {
		return err
	}

	err = s.redis.Set(context.TODO(), "vacancy_list", data, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

// clearCache очищает кеш
func (s *PostgresService) clearCache() {
	_ = s.redis.FlushAll(context.TODO()).Err()
}

package selenium

import (
	"parser/internal/domain"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /vacancies vacancy createVacancyRequest
// Create a new vacancy.
// responses:
//
//	201: createVacancyResponse
//	400: errorResponse
//	500: errorResponse
//
// swagger:parameters createVacancyRequest
type createVacancyRequest struct {
	// Vacancy object to create
	// in: body
	Body domain.Vacancy
}

// swagger:response createVacancyResponse
type createVacancyResponse struct {
	// Created vacancy object
	// in: body
	Body domain.Vacancy
}

// swagger:route DELETE /vacancies/{id} vacancy deleteVacancyRequest
// Delete a vacancy by ID.
// responses:
//
//	200: deleteVacancyResponse
//	404: errorResponse
//
// swagger:parameters deleteVacancyRequest
type deleteVacancyRequest struct {
	// ID of the vacancy to delete
	// in: path
	ID string `json:"id"`
}

// swagger:response deleteVacancyResponse
type deleteVacancyResponse struct {
	// in: body
	Body string
}

// swagger:route GET /vacancies vacancy getListRequest
// Get the list of vacancies.
// responses:
//
//	200: getListResponse
//	500: errorResponse
//
// swagger:parameters getListRequest
type getListRequest struct {
	// No parameters
}

// swagger:response getListResponse
type getListResponse struct {
	// in: body
	Body []domain.Vacancy
}

// swagger:route GET /vacancies/{id} vacancy getByIDRequest
// Get a vacancy by ID.
// responses:
//
//	200: getByIDResponse
//	404: errorResponse
//
// swagger:parameters getByIDRequest
type getByIDRequest struct {
	// ID of the vacancy to get
	// in: path
	ID string `json:"id"`
}

// swagger:response getByIDResponse
type getByIDResponse struct {
	// in: body
	Body domain.Vacancy
}

// swagger:response errorResponse
type errorResponse struct {
	// in: body
	Body struct {
		Error string `json:"error"`
	}
}

// swagger:route POST /login auth LoginHandler
//
// Авторизация пользователя.
//
// # Авторизует пользователя по переданным данным из JSON - запроса, возвращает токен
//
// Responses:
//
//	200: loginResponse
//	400: errorResponse
//	401: errorResponse
//
// swagger:parameters LoginHandler
type loginHandler struct {
	// Входные данные для авторизации
	//
	// Required: true
	// in: body
	Body struct {
		// Email пользователя
		// Example: user@example.com
		Email string `json:"email" binding:"required"`

		// Пароль пользоваетеля
		// Example: mypass
		Password string `json:"password" binding:"required"`
	}
}

// Ответ при успешной авторизации.
//
// swagger:response loginResponse
type loginResponse struct {
	// Токен доступа для авторизованного пользователя.
	// in: body
	Body struct {
		// Пример: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMTIzNDU2Nzg5MCIsImVtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImlhdCI6MTYyNDM5NzUzOCwiZXhwIjoxNjI0Mzk3NjU4LCJpc3MiOiJhdXRoZW50aWNhdGlvbiJ9.CFey4t6JdwtocHEmS8vDEi7Wm0rCpD8pGeNcp3U8fQk"
		AccessToken string `json:"accessToken"`
	}
}

// Ответ при ошибке авторизации.
//
// swagger:response errorAuthResponse
type errorAuthResponse struct {
	// Текст ошибки.
	// in: body
	Body struct {
		// Пример: "Неправильный email или пароль"
		Error string `json:"error"`
	}
}
